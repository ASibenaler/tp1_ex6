## Exercice 6

# Objectif
L'objectif de cet exercice est de mettre en place un ensemble de tests sur la classe SimpleComplexCalculator, avec *unittest*.

# Réalisation
On créé dans le fichier de test quatre classe de test correspondant à chaque opération.
Chacune de ces classe contient une méthode d'initialisation, instanciant la classe SimpleComplexCalculator, puis une méthode par test à effectuer. On test chaque opération en entrant une liste d'entiers, une liste de flotants, et une liste contenant un caractère.

# Lancement
On peut lancer le programme de test en l'exécutant, ou en utilisant *pytest* :
	pytest Test/ex6.py

# Notes
Les tests efféctués avec des flotants échouent. En effet, lorsque l'on effectue des calculs avec des flotants, Python renvoie une valeur aproximée à 10^-15 près :

	FAILED Test/ex6.py::Test_sum::test_sum_float - AssertionError: Lists differ: [4.4, 6.6000000000000005] != [4.4, 6.6]
	FAILED Test/ex6.py::Test_substract::test_sub_float - AssertionError: Lists differ: [-2.1999999999999997, -2.2] != [-2.2, -2.2]
	FAILED Test/ex6.py::Test_multiply::test_mul_float - AssertionError: Lists differ: [-6.050000000000002, 12.100000000000001] != [...
	FAILED Test/ex6.py::Test_divide::test_div_float - AssertionError: Lists differ: [0.44000000000000006, 0.4] != [0.44, 0.4]

On peut donc considérer que ces test sont réussis.
